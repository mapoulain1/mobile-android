package com.max.tp1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.max.tp1.model.Item
import com.max.tp1.viewmodel.ItemViewModel
import java.util.UUID


class CameraFragment : Fragment() {

    //private var items = ArrayList<Item>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //val saved = savedInstanceState?.getSerializable("values")
        //if(saved != null)
        //    items= saved as ArrayList<Item>
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //outState.putSerializable("values", items)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_camera, container, false)

        root.findViewById<Button>(R.id.button4).setOnClickListener{
            view?.findNavController()?.navigate(R.id.action_cameraFragment_to_mainFragment)
        }

        val viewModel: ItemViewModel by viewModels()

        val recyclerView = root.findViewById<View>(R.id.recycler_view) as RecyclerView

        val adapter = CustomAdapter(viewModel.items)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)

        root.findViewById<Button>(R.id.button3).setOnClickListener {
            viewModel.items.add(Item(UUID.randomUUID().toString()))
            adapter.notifyItemInserted(viewModel.items.size - 1)
        }

        return root
    }


}