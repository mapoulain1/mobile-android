package com.max.tp1

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.max.tp1.model.Item

class CustomAdapter(private val dataSet: MutableList<Item>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView
        val selectItem: Button
        val removeItem: Button

        init {
            // Define click listener for the ViewHolder's View
            textView = view.findViewById(R.id.textView)
            selectItem = view.findViewById(R.id.selectItem)
            removeItem = view.findViewById(R.id.buttonRemove)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.text_row_item, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.selectItem.setOnClickListener {
            dataSet.get(position).selected = !dataSet.get(position).selected;
            notifyItemChanged(position)
        }

        viewHolder.removeItem.setOnClickListener {
            val item = dataSet.get(position)
            dataSet.remove(item)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, dataSet.size)

        }

        viewHolder.textView.text = dataSet.get(position).value + (if (dataSet.get(position).selected) " V" else " X")
        viewHolder.textView.setBackgroundColor(if (dataSet.get(position).selected) Color.RED else Color.BLACK)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

}
