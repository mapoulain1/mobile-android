package com.max.tp1


import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("ON CREATE","")

        supportActionBar?.hide()

        window.apply {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("ON START","")
    }

    override fun onStop() {
        super.onStop()
        Log.d("ON STOP","")
    }

    override fun onResume() {
        super.onResume()
        Log.d("ON RESUME","")
    }

    override fun onPause() {
        super.onPause()
        Log.d("ON PAUSE","")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("ON DESTROY","")
    }


}