package com.max.tp1

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)

        val button = root.findViewById<Button>(R.id.button)
        val buttonCamera = root.findViewById<Button>(R.id.button2)
        val text = root.findViewById<TextView>(R.id.textView)
        val editText = root.findViewById<EditText>(R.id.editText)

        button?.setOnClickListener {
            text?.text = editText?.text
            Toast.makeText(root.context, R.string.main_activity_button_toast, Toast.LENGTH_SHORT)
                .show()
        }

        buttonCamera?.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_mainFragment_to_cameraFragment)
        }


        return root
    }


}